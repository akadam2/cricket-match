# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
super_admin_role = Role.create(name: "SuperAdmin")
admin_role = Role.create(name: "Admin")

AdminUser.create!(role_id: super_admin_role, email: 'super_admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
AdminUser.create!(role_id: admin_role, email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

100.times do |value|
  Player.create(
    first_name: Faker::Name.first_name,
    middle_name: Faker::Name.middle_name,
    last_name: Faker::Name.last_name,
    gender: Faker::Gender.binary_type,
    birth_date: Faker::Date.birthday,
    contact: Faker::PhoneNumber.cell_phone,
    speciality: rand(0..3),
    is_captain: rand(0..1),
    is_active: Faker::Boolean.boolean
  )
end


puts '-----------------'
puts "Seed file successfully load."
puts '-----------------'