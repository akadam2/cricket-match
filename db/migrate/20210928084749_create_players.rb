class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :gender
      t.date :birthdate
      t.string :contact
      t.text :address
      t.integer :speciality
      t.integer :is_captain
      t.boolean :is_active

      t.timestamps
    end
  end
end
