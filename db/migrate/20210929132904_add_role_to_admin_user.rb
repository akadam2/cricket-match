class AddRoleToAdminUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :admin_users, :role, index: true, foreign_key: true
  end
end
