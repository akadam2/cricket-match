class CreateMatches < ActiveRecord::Migration[6.0]
  def change
    create_table :matches do |t|
      t.datetime :schedule
      t.boolean :is_draw

      # t.references :creator, references: :users, foreign_key: { to_table: :users}
      t.references :first_team, references: :teams, foreign_key: { to_table: :teams}
      t.references :second_team, references: :teams, foreign_key: { to_table: :teams}
      t.references :winner_team, references: :teams, foreign_key: { to_table: :teams}
      t.references :toss_winner_team, references: :teams, foreign_key: { to_table: :teams}



      t.timestamps
    end
  end
end
