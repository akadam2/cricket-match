class RenameBirthdateToBirthDateInPlayers < ActiveRecord::Migration[6.0]
  def change
    rename_column :players, :birthdate, :birth_date
  end
end
