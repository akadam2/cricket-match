class CreateTeamsPlayersJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :teams, :players do |t|
      # t.references :teams, null: false, foreign_key: true
      # t.references :players, null: false, foreign_key: true
      t.index :team_id
      t.index :player_id
    end
  end
end
