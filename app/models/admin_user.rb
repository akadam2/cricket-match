class AdminUser < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :validatable
  belongs_to :role

  def super_admin?
    role.name == "SuperAdmin"
  end

  def admin?
    role.name == "Admin"
  end

end