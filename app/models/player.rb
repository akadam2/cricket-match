class Player < ApplicationRecord
  # belongs_to :team
  has_and_belongs_to_many :teams

  enum speciality: {batsman: 0, bowler: 1, fielder:2, all_rounder: 3}
  enum is_captain: ["Yes", "No"]

  def full_name
    "#{first_name} #{last_name}"
  end
end
