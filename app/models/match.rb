class Match < ApplicationRecord
  belongs_to :team, class_name: 'Team', foreign_key: :first_team_id
  belongs_to :team, class_name: 'Team', foreign_key: :second_team_id
  belongs_to :team, class_name: 'Team', foreign_key: :winner_team_id
  belongs_to :team, class_name: 'Team', foreign_key: :toss_winner_team_id
end
