class PlayerMailer < ApplicationMailer

  def new_player
    @player = params[:player]
    mail(to: @player.email, subject: "Welcome New Player")
  end
end
