class MatchMailer < ApplicationMailer

	def notification
	  @player = params[:player]
	  @schedule = params[:schedule]
	  mail(to: @player.email, subject: "Match Schedule #{@schedule}")
	end

end
