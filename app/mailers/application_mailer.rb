class ApplicationMailer < ActionMailer::Base
  default from: 'akadam@deqode.com'
  layout 'mailer'
end
