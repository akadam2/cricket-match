class ApplicationController < ActionController::Base
  # before_action :authenticate_user!

  def access_denied(exception)
    redirect_to admin_organizations_path, alert: exception.message
  end

end