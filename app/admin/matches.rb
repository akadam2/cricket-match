ActiveAdmin.register Match do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :schedule, :is_draw, :first_team_id, :second_team_id, :toss_winner_team_id, :winner_team_id

  index do
    selectable_column
    id_column
    column :schedule
    column :first_team_id
    column :second_team_id
    column :winner_team_id
    column :toss_winner_team_id
    column :is_draw
    actions
  end

  filter :schedule
  filter :winner_team_id
  filter :is_draw

  form do |f|
    f.inputs do
      f.input :first_team_id, as: :select, collection: Team.all.map{|x| [x.name, x.id]}, prompt: "Select Team"
      f.input :second_team_id, as: :select, collection: Team.all.map{|x| [x.name, x.id]}, prompt: "Select Team"
      f.input :toss_winner_team_id, as: :select, collection: Team.all.map{|x| [x.name, x.id]}, prompt: "Select Team"
      f.input :winner_team_id, as: :select, collection: Team.all.map{|x| [x.name, x.id]}, prompt: "Select Team"

      f.input :schedule, as: :datetime_picker, datetime_picker_options: { min: Date.today, max: "+3D" }
      f.input :is_draw
    end
    f.actions
  end
end
