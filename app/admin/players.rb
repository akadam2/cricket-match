ActiveAdmin.register Player do
  permit_params :first_name, :middle_name, :last_name, :email, :gender, :birth_date, :contact, :address, :speciality, :is_captain, :is_active

  index do
    selectable_column
    id_column
    column :first_name
    column :middle_name
    column :last_name
    column :email
    column :gender
    column :birth_date
    column :contact
    column :speciality
    column :is_captain
    column :is_active
    actions
  end

  filter :first_name
  filter :last_name
  filter :email
  filter :gender
  filter :birth_date

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :middle_name
      f.input :last_name
      f.input :email
      f.input :gender, collection: ["Male", "Female", "Other"], prompt: "Select Gender"
      f.input :birth_date, as: :datepicker, datepicker_options: { min_date: Date.today-100.years, max_date: "+3D" }

      f.input :contact
      f.input :address
      f.input :speciality, as: :select, collection: Player.specialities.keys, prompt: "Select Speciality"
      f.input :is_captain, prompt: "Select Captain"
      f.input :is_active
    end
    f.actions
  end

  after_create do |player|
    @player = player
    PlayerMailer.with(player: @player).new_player.deliver_later
  end
end