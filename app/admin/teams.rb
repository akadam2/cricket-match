ActiveAdmin.register Team do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :address, :player_ids
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :address]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end


  index do
    selectable_column
    id_column
    column :name
    column :address
    actions
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :address
      f.input :player_ids, as: :check_boxes, collection: Player.all.map{|x| [x.first_name, x.id]}
    end
    f.actions
  end

  after_save do |team|
    team.players.delete_all if team.players.present?
    player_ids = params[:team][:player_ids].reject!(&:empty?)
    player_ids.each do |player_id|
      team.players << Player.find(player_id)
    end
  end
  
end
