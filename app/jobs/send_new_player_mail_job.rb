class SendNewPlayerMailJob < ApplicationJob
  queue_as :default

  def perform(*args)
    PlayerMailer.with(player: args[0]).new_player.deliver_later
  end

end
