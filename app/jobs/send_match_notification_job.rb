class SendMatchNotificationJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Do something later
    date = Time.zone.now + 1.day
    matches = Match.where(schedule: date.all_day)
    matches.each do |match|
      @schedule = match.schedule
      first_team = Team.find(match.first_team_id)
      second_team = Team.find(match.second_team_id)
      players = first_team.players + second_team.players
      players.each do |player|
        MatchMailer.with(player: @player, schedule: @schedule).new_player.deliver_later
      end
      puts "----Match notification send.----"
    end
  end
end
